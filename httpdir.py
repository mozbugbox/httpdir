#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import io
import re
import logging

import json
import time
import mimetypes
import urllib.parse as uparse
import pathlib

try:
    from gevent import pywsgi
    from gevent.pool import Pool
    space_re = re.compile(r"\s+")
    class MyWSGIHandler(pywsgi.WSGIHandler):
        def read_request(self, raw_requestline):
            """ Override pyswgi to fix path with whitespace(' ') in it """
            method, _, rest = raw_requestline.rstrip().partition(" ")
            path, sep, version = rest.rpartition(" ")
            if not version.startswith("HTTP/"):
                path = rest
                sep = ""

            if space_re.search(path):
                path = uparse.quote(path, encoding=HEADER_ENCODE)

            words = [method, path] if not sep else [method, path, version]
            raw_requestline = " ".join(words)
            return pywsgi.WSGIHandler.read_request(self, raw_requestline)

except ImportError as e:
    print(e.msg + ". Please install the missing module.")
    from wsgiref.simple_server import make_server

def has_gevent():
    return "pywsgi" in globals()

__version__ = "1.1"
SERVER_STRING="httpdir/{}".format(__version__)
KEY_FILE="hd.server.key"
CERT_FILE="hd.server.crt"
KEY_FILE=None
CERT_FILE=None

DEFAULTS = {
        "ip": "0.0.0.0",
        "port": 8088,
        "root_dir": "/tmp/webfs",
        "upload_dir": "",
        "vhost_off": False,
        }

NATIVE=sys.getfilesystemencoding()

HEADER_ENCODE = "iso-8859-1"
UPLOAD_PATH = "/upload-result"

html_header = """\
<!DOCTYPE html><html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
      table.dirList td {{
        padding-right: 2em;
      }}
      table.dirList th {{
        background-color:#A7C942;
        color:#ffffff;
      }}
      td.num {{
        text-align: right;
        padding-right: 1em;
        padding-left: 1em;
      }}
  </style>
  <title>{}</title>
"""

template_list_dir = html_header + """\
</head>
<body>
  <h1>Directory: {path:}</h1>
"""

template_404 = html_header + """\
</head>
<body>
  <h1>404 - Not Found</h1>
  Request: <a href="/">{hostname:}</a>{path}<p/>
</body></html>
"""

def copy_file_content(from_fp, to_fp, block_size):
    count = 0
    while True:
        data = from_fp.read(block_size)
        if not data:
            break
        to_fp.write(data)
        count += len(data)
    return count

def num2human(num, kunit=1000):
    """Convert integer to human readable units"""
    units = ["K", "M", "G", "T"]
    ret = "{}".format(num)
    num = float(num)

    for i in range(len(units)-1, -1, -1):
        div = kunit**(i+1)
        if (num/div) > 1:
            ret = "{:.2f}{}".format(num/div, units[i])
            break
    return ret

def umask2mode(mask):
    """Convert umask from 644 to rw-r--r-"""
    m = []
    access = "xwr" # reverse at the end
    for i in range(3):
        for j in range(3):
            c = "-"
            if mask & 1 << (i*3 + j):
                c = access[j]
            m.append(c)
    m.reverse()
    return "".join(m)

def is_binary(filename, head_size = 1024):
    """Test if a file is binary"""
    with io.open(filename, "rb") as fd:
        head = fd.read(head_size)
    ret = b'\0' in head
    return ret

def gen_table_row(f, base_path, full_fname):
    """Generate a <tr> element of file path"""
    link_path = os.path.join(base_path, f)
    stat = os.stat(full_fname)
    # posix.stat_result(st_mode=16832, st_ino=5767169, st_dev=2049L,
    #   st_nlink=2, st_uid=0, st_gid=0, st_size=4096,
    #   st_atime=1358911565, st_mtime=1382665530, st_ctime=1382665530)
    #print(f, umask2mode(stat.st_mode))
    dir_char = ""
    fsize_human = num2human(stat.st_size) + "B"

    if os.path.isdir(full_fname):
        dir_char = "/"
        fsize_human = "-"
    gt = time.gmtime(stat.st_mtime)
    time_str = time.strftime("%d/%m/%y %H:%M", gt)
    fsize = stat.st_size
    line = ['<tr>']
    line.append(f'<td><a href="{link_path}">{f}{dir_char}</a></td>')
    line.append(
            f'<td class="num" title="{fsize:,}">{fsize_human}</td>')
    line.append(f"<td>{time_str}</td>")
    line.append("</tr>")
    return line

class PostError(Exception):
    pass
disp_re = re.compile(r'Content-Disposition.*name="file"; filename="(.*)"')

class WHandler:
    """wsgi application handler"""
    def __init__(self, root_dir, options):
        self.root = root_dir
        self.options = options
        self.block_size = 1024 * 64
        self.vhost = True
        self.umask = 0o700

    def set_vhost(self, v):
        self.vhost = v

    def __call__(self, env, start_response):
        """
        @env: Dict = {
                         REQUEST_METHOD
                         SCRIPT_TIME
                         PATH_INFO
                         QUERY_STRING
                         CONTENT_TYPE
                         CONTENT_LENGTH
                         SERVER_NAME # Prefered to use HTTP_HOST if exist
                         SERVER_PORT
                         SERVER_PROTOCOL # HTTP/1.0 or 1.1
                         REMOTE_ADDR
                         REMOTE_PORT
                         HTTP_Variables...

                         wsgi.version # tuple (1, 0)
                         wsgi.url_scheme
                         wsgi.input
                         wsgi.errors
                         wsgi.multithread
                         wsgi.multiprocess
                         wsgi.run_once
                         wsgi.file_wrapper # optional (send_file)
                                           # func(file_obj, block_size)
                     }
        @start_response: func(status, response_headers, exc_info=None)
                         return -> a write(body_data) callable
        @return: iterable of bytestrings.
                 Optional __len__(iterable) for content-length
                 Optional iterable.close() to be called on finalize
        """
        NEED_DECODE = ["PATH_INFO"]
        for k in env:
            if k.startswith("HTTP_") or k in NEED_DECODE: # decode headers
                v = env[k].encode(HEADER_ENCODE).decode("UTF-8")
                env[k] = v
        log.debug("ENV: {}".format(env))
        if env["REQUEST_METHOD"] == "POST":
            return self.do_post(env, start_response)

        if "HTTP_HOST" in env:
            hostname = env["HTTP_HOST"].partition(":")[0]
        else:
            hostname = env["SERVER_NAME"]
        if self.vhost:
            full_root = os.path.join(self.root, hostname)
        else:
            full_root = self.root

        log.debug("full_root: {}".format(full_root))
        if not os.path.exists(full_root):
            return self.not_found(env, start_response)
        if not mimetypes.inited:
            mimetypes.init()

        full_path = os.path.join(full_root, env["PATH_INFO"].lstrip("/"))
        log.debug("full_path: {}".format(full_path))

        exist_file = self.check_file(full_path)
        if exist_file:
            full_path = exist_file
        else:
            return self.not_found(env, start_response)
        com_prefix = os.path.commonprefix([self.root, full_path])
        if com_prefix != self.root:
            return self.not_found(env, start_response)
        elif os.path.isdir(full_path):
            return self.list_directory(full_path, full_root,
                    env, start_response)
        else:
            return self.transfer_file(full_path, env, start_response)

    def do_form(self, form, env):
        """Handle post form, really just upload file"""
        for k in form.keys():
            item = form[k]
            if item.filename:
                path = self.translate_upload_path(item.filename)
                log.info('{} - - Uploading "{}" to "{}"'.format(
                    env["REMOTE_ADDR"], item.filename, path))
                with io.open(path, "wb") as fhw, item:
                    fsize = copy_file_content(item.file, fhw, self.block_size)
                    self.upload_files.append((item.filename, fsize))

            if item.type.startswith("multipart/"):
                self.do_form(item)

    def translate_upload_path(self, fname):
        """Get full path in local upload directory"""
        fname = pathlib.Path(fname).name
        fname = fix_filename(fname)
        updir = pathlib.Path(self.options["upload_dir"])
        updir.mkdir(mode=self.umask, parents=True, exist_ok=True)
        path = updir / fname
        stem = path.stem
        suffix = path.suffix

        n = 1
        while path.exists():
            path = path.with_name("{}-{}{}".format(stem, n, suffix))
            n += 1
        return path

    def do_post(self, env, start_response):
        """Handle post request"""
        if env["PATH_INFO"] == UPLOAD_PATH:
            return self.do_upload(env, start_response)
        else:
            return self.not_found(env, start_response)

    def do_upload(self, env, start_response):
        """Handle File upload"""
        import cgi
        self.upload_files = []
        form0 = cgi.FieldStorage(fp=env["wsgi.input"], environ=env)
        self.do_form(form0, env)

        msg = [html_header.format("Upload Result"),
                "</head><body><h2>Upload Result</h2>",
                '<table class="dirList">',
                '<tr><th>Name</th><th>Size</th></tr>']
        for v in self.upload_files:
            msg.append(f'<tr><td>{v[0]}</td><td class="num">{v[1]}</td></tr>')
        msg.append("</table></body><html>")
        msg = "\n".join(msg).encode("UTF-8")

        start_response("200 OK", [('Content-Type', 'text/html')])
        return [msg]

    def check_file(self, path):
        """Check existent of file, vlc quote_plus URL fuck it!"""
        ret = None
        if os.path.exists(path):
            return path

        path_u = uparse.unquote_plus(path)
        if os.path.exists(path_u):
            ret = path_u
        return ret

    def feed_file(self, filename, byte_range=None):
        """Feed content of a file"""
        filesize = os.path.getsize(filename)

        start = 0
        end = filesize
        if byte_range is not None:
            start = byte_range[0]
            # byte_range count start from 0
            end = min(byte_range[1] + 1, filesize)
        with io.open(filename, 'rb') as fd:
            if start > 0:
                fd.seek(start, os.SEEK_SET)

            cur = start
            block_size = self.block_size
            while cur < end:
                if (end - cur) < self.block_size:
                    block_size = end - cur
                data = fd.read(block_size)
                cur += len(data)
                if data:
                    yield data
                else:
                    break

    def transfer_file(self, full_path, env, start_response):
        """transfer a static file"""
        range_string = None
        fsize = None

        urlcode = 200

        feed_range = None
        feed_filename = full_path

        fsize = os.path.getsize(full_path)
        content_len = fsize # could change with range request
        if "HTTP_RANGE" in env:
            _, _s, r = env["HTTP_RANGE"].partition("=")
            bstart, _, bend = r.partition("-")
            bstart = 0 if len(bstart) == 0 else int(bstart)
            bend = fsize - 1 if len(bend) == 0 else int(bend)
            if bend >= fsize:
                bend = fsize - 1
            urlcode = 206
            content_len = bend - bstart + 1
            range_string = "bytes {}-{}/{}".format(
                    bstart, bend, fsize)
            feed_range = [bstart, bend]

        mtype = mimetypes.guess_type(full_path, False)[0]
        if mtype is None:
            mtype = 'application/octet-stream' if is_binary(
                    feed_filename) else 'text/plain'

        # Date: Thu, 14 Nov 2013 10:35:03 GMT
        mt = time.gmtime(os.path.getmtime(full_path))
        gt = time.strftime("%a, %d %b %Y %H:%M:%S GMT", mt)

        fname = os.path.basename(full_path)
        fname_url = uparse.quote(fname.encode("UTF-8"))

        headers = [
                   ('Server', SERVER_STRING),
                   ('Content-Type', mtype),
                   ('Content-Length', str(content_len)),
                   ('Last-Modified', gt),
                   ('Accept-Ranges', 'bytes'),
                ]
        if has_gevent(): # hop-by-hop not allowed by wsgiref
            headers.extend([('Connection', 'Keep-Alive')])
        if mtype in {'application/octet-stream',}:
            dispo = "attachment; filename*=utf-8''{}".format(fname_url)
            headers.append(('Content-Disposition', dispo))

        if range_string:
            headers.append(('Content-Range', range_string))

        log.debug("Response: {}".format(headers))
        start_response("{} OK".format(urlcode), headers)

        feeder = self.feed_file(feed_filename, feed_range)
        return feeder

    def list_directory(self, full_dir, full_root, env, start_response):
        base_path = env["PATH_INFO"]
        title = "Dir " + base_path
        header = template_list_dir.format(title, path=base_path)
        msg = [header]
        msg.append('<table class="dirList">\n<tr><th>Name</th>' +
                '<th>Size</th><th>Modified</th></tr>')
        m1 = ['<tr><td><a href="..">../</a></td></tr>']
        m2 = []
        total_size = 0
        file_list = os.listdir(full_dir)

        if "filter" in self.options:
            import fnmatch
            file_list = fnmatch.filter(file_list, *self.options["filter"])
        for f in sorted(file_list, key=lambda x: x.casefold()):
            full_fname = os.path.join(full_dir, f)
            line = gen_table_row(f, base_path, full_fname)
            if os.path.isdir(full_fname):
                m1.append("\n".join(line))
            else:
                m2.append("\n".join(line))
                total_size += os.path.getsize(full_fname)

        msg.extend(m1)
        msg.extend(m2)
        msg.append("</table>")
        total_human = num2human(total_size) + "B"
        msg.append('<hr/><span title="{:,}">Total [{}]: {}</span><p/>'.format(
            total_size, len(file_list), total_human))

        msg.append("<h3>Upload Files:</h3><ul>")
        msg.append('<form ENCTYPE="multipart/form-data" '
                'method="post" action={}'.format(UPLOAD_PATH))
        for i in range(10):
            msg.append(f'<li><input name="file_{i:02d}" type="file"/></li>')

        msg.append('</ul><input type="submit" value="Upload"/></form>')
        msg.append("</body></html>")

        start_response("200 OK", [('Content-Type', 'text/html')])
        msg = "\n".join(msg).encode("UTF-8")
        return [msg]

    def not_found(self, env, start_response):
        if "HTTP_HOST" in env:
            hostname = env["HTTP_HOST"].partition(":")[0]
        else:
            hostname = env["SERVER_NAME"]

        start_response('404 Not Found', [('Content-Type', 'text/html')])
        title = '404 - Not Found@' + hostname
        path = env["PATH_INFO"]
        msg = template_404.format(title, hostname=hostname, path=path)
        msg = msg.encode("UTF-8")
        return [msg]

def fix_path(apath):
    """For a path string, expand user, variables and normalize it"""
    new_path = os.path.expanduser(apath)
    new_path = os.path.expandvars(new_path)
    new_path = os.path.normpath(new_path)
    return new_path

def fix_filename(fname):
    """strip uncommon chars from filename"""
    fname = re.sub(r'["&|;\\/?^$]', "-", fname)
    return fname

def load_config(cfile, arg_keys=[]):
    config = {}
    if not os.path.exists(cfile):
        return config

    log.debug('loading config file "{}"...'.format(cfile))
    import configparser
    conf = configparser.ConfigParser()
    with io.open(cfile, "r", encoding="UTF-8") as fd:
        conf.readfp(fd)

    sect = "main"
    option_ks = conf.options(sect)
    used = set()
    for k in arg_keys: # unify cmd args and config options: "_" -> ""
        kc = k.replace("_", "")
        if kc not in option_ks:
            continue
        v = conf.get(sect, kc).strip()
        config[k] = v
        used.add(kc)

    for k, v in conf.items(sect):
        if k not in used:
            config[k] = v

    log.debug("config: {}".format(str(config)))
    return config

def load_options(args=None):
    """Load options from cmdline args and config file"""
    options = {}
    if args is None:
        arg_parser = setup_arg_parser()
        args = arg_parser.parse_args()

    arg_keys = list(args.__dict__.keys())
    arg_keys.remove("config_file")
    if args.config_file:
        cfile = fix_path(args.config_file)
        config = load_config(cfile, arg_keys)
        options.update(config)

    for k in arg_keys: # merge cmdline args
        v = getattr(args, k)
        if v is not None:
            options[k] = v

    if "DEFAULTS" in globals(): # Load defaults
        defaults = DEFAULTS
        for k in defaults.keys():
            if  options.get(k, None) is None:
                options[k] = defaults[k]

    return args, options

def process_options(args, dump_options=True):
    _, options = load_options(args)
    #print(options); return

    def fix_bool_options(akey):
        """Convert bool option from text form. As see in config file."""
        v = options[akey]
        true_v = {"1", "true", "yes", "on"}
        if not isinstance(v, bool):
            options[akey] = True if v.lower in true_v else False

    bool_options = ["vhost"]
    int_options = ["port"]
    json_options = ["filter"]

    tmp_dict = {k: int(options[k]) for k in int_options if k in options}
    options.update(tmp_dict)

    [fix_bool_options(k) for k in bool_options if k in options]
    for ckey in json_options:
        if ckey in options and isinstance(options.get(ckey, None), str):
            options[ckey] = json.loads(options[ckey])

    if not options["upload_dir"]:
        options["upload_dir"] = pathlib.Path(options["root_dir"]) / "upload"

    if dump_options:
        local_options = options.copy()
        if local_options["ip"] == "0.0.0.0":
            real_ips = ", ".join(get_interface_ips())
            local_options["ip"] = "{} ({})".format(options["ip"], real_ips)

        print("Config: {}".format(args.config_file))
        print("""[main]
RootDir = {root_dir}
UploadDir = {upload_dir}
IP = {ip}
Port = {port}
Vhost = {vhost}
""".format(**local_options))

        for ckey in json_options:
            if ckey in options:
                v = json.dumps(options[ckey])
                print("{} = {}".format(ckey, v))
    return options

def get_interface_ips():
    """fetch ip addresses of the local machine"""
    try:
        import socket
        import psutil
        addrs = psutil.net_if_addrs()
        ips = [y.address for x in addrs.values()
                for y in x if y.family == socket.AddressFamily.AF_INET]
    except ImportError:
        import subprocess
        txt = subprocess.getoutput("ip -4 -br address")
        ips = re.findall(r"((?:\d{1,3}\.){3}\d{1,3})/", txt)
    return ips
#print(get_interface_ips()); sys.exit()

def setup_server(options):
    ip = options["ip"]
    port = options["port"]
    root_dir = fix_path(options["root_dir"])
    vhost = options["vhost"]
    log.debug("options: {}".format(options))

    http_handler = WHandler(root_dir, options)
    http_handler.set_vhost(vhost)

    if has_gevent():
        server = pywsgi.WSGIServer((ip, port), http_handler, spawn=100,
                handler_class=MyWSGIHandler)
    else:
        server = make_server(ip, port, http_handler)
    return server

def setup_arg_parser(appname=None):
    if not appname:
        appname = os.path.basename(sys.argv[0])
        if appname.endswith(".py"):
            appname = appname[:len(appname)-3]

    import argparse
    parser = argparse.ArgumentParser(
            description="Server local file system to web",
            prefix_chars = "-+")
    parser.add_argument("--version", action="version",
            version="%(prog)s {}".format(__version__))
    parser.add_argument("-r", "--root-dir", type=str,
        help="root directory of the server. Default: {}".format(
            DEFAULTS["root_dir"]))
    parser.add_argument("-u", "--upload-dir", type=str,
        help="upload file directory of the server. Default: {}".format(
            "root-dir/upload"))
    parser.add_argument("+f", "--filter", type=str, action="append",
            help="Only list filtered files")
    parser.add_argument("+v", "--vhost", dest="vhost", action="store_true",
        help="virtual host. Use hostname as root subdir.")
    parser.add_argument("-i", "--ip", type=str,
            help="interface address of the server. Default: {}".format(
                DEFAULTS["ip"]))
    parser.add_argument("-p", "--port", type=int,
            help="Port of the server. Default: {}".format(
                DEFAULTS["port"]))

    default_config = "${{HOME}}/.config/{}/config".format(appname.lower())
    parser.add_argument("-c", "--config-file", type=str,
            default=default_config,
            help="The config file to load. Default: {}".format(
                default_config))
    parser.add_argument("-D", "--debug", action="store_true",
            help="Debug run")

    return parser

def setup_log(log_level):
    global log
    log = logging.getLogger(__name__)
    log.setLevel(log_level)
    ch = logging.StreamHandler()
    ch.setLevel(log_level)
    formatter = logging.Formatter("%(levelname)s>> %(message)s")
    ch.setFormatter(formatter)
    log.addHandler(ch)

def main(url=None):
    appname = "httpdir"
    parser = setup_arg_parser(appname)
    args = parser.parse_args()

    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.DEBUG if args.debug else logging.INFO
    setup_log(log_level)

    options = process_options(args)
    server = setup_server(options)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        if hasattr(server, "shutdown"):
            server.shutdown()
        elif hasattr(server, "close"):
            server.close()
        print("User quit")

if __name__ == '__main__':
    main()

